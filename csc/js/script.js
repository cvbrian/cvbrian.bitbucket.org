(function ($j) {

  switch_style = {

    onReady: function () {
      this.switch_style_click();
    },

    switch_style_click: function(){
        $(".box").on ('click', function(){
            var id = $(this).attr("id");
            $("#switch_style").attr("href", "https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/" + id + "/bootstrap.min.css");
            $("#currentTheme").text(id);
            $('#themeModal').modal('hide');
        });
    },
  };

  $j().ready(function () {
      switch_style.onReady();
  });

})(jQuery);
